-- UART Project - Transmitter
-- Daniel Borquaye - DB363
-- 28/11/2014

----------------------------------------------------------
-- Library Statements
----------------------------------------------------------

library ieee;
    
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_unsigned.all;

----------------------------------------------------------
-- Entity Declaration
----------------------------------------------------------

entity TX is 

  port (
    START_TX, CE, CLK, RES : in std_logic;
    D                      : in std_logic_vector(7 downto 0);

    UART_TX, TX_DONE            : out std_logic);
  end TX;
        
----------------------------------------------------------
-- Architecture Declaration
----------------------------------------------------------
          
architecture functional of TX is

----------------------------------------------------------
-- Signal Declarations
----------------------------------------------------------
signal START_SYNC, Q1, Q2                  : std_logic;
signal TX_REG                              : std_logic_vector(7 downto 0);
signal Q                                   : std_logic_vector(2 downto 0);
signal BIT_CNTR                            : integer range 0 to 8;
signal OS_CNTR                             : integer range 0 to 16; 

-- Declaration of the 5 states the system can enter in the state machine
type xx_state is (IDLE, START, DATA, PARITY, STOP);

-- Sets the initial state to IDLE. 
signal state : xx_state := IDLE;


begin

-- Process to synchronise the press of the start button to the clock
-- A 3 bit register has been used to shift the signal in on the clock
-- The extra bit in the register ensures the signal doesnt stay high
Sync: process(CE)
   begin
    if rising_edge(CE) then
     if RES = '1' then
      Q <= "000";
     else
-- Shifting
      Q(1 downto 0) <= Q(2 downto 1);
      Q(2)          <= START_TX;
      Q1            <= Q(1);
      Q2            <= not Q(0);
       if (Q1 = '1') and (Q2 = '1') then         -- An AND gate is placed at the ouput of the register comparing the middle bit with NOT the 1st bit
        START_SYNC <= '1';                       -- The output to the gate will be high when the signal has gone from low to high
       else
        START_SYNC <= '0';
       end if;

     end if;

   end if;
end process;

-- State machine process. The process uses a signal OS_CNTR which is used to oversample the data to make sure it is stable
State_Machine: process(CLK) 
   begin
   if rising_edge(CLK) then
    if RES = '1' then
     state <= IDLE;
     TX_REG <= "11111111";
    end if;
    if CE = '1' then
      case state is
     -- Setting the signal values for the idle state, setting all the counters to 0 and tx output to 1 as the receiver starts on an active low signal
        when IDLE =>
         OS_CNTR  <= 0;
         BIT_CNTR <= 0;
         TX_DONE  <= '0';
         UART_TX  <= '1';
         if START_SYNC = '1' then
          state <= START;
         end if;
    -- Load input data onto a temporary register and begin counter. set uart_tx low (active low start bit)
        when START =>
          TX_REG <= D;
          UART_TX     <= '0';
          OS_CNTR <= OS_CNTR + 1;
         if OS_CNTR = 15 then
          OS_CNTR <= 0;
          state  <= DATA;
         end if;
    -- Sample the lowest bit in the temp register and place the value on the uart_tx output 
        when DATA =>       
         UART_TX       <= TX_REG(0);
         OS_CNTR  <= OS_CNTR + 1;
         if OS_CNTR = 15 then
          if BIT_CNTR = 7 then
            OS_CNTR <= 0;
            state <= PARITY;
    -- If the number of bits pushed out is not 7 shift another bit right and replace the highest bit with a 1
           elsif BIT_CNTR /= 7 then
            OS_CNTR  <= 0;
            TX_REG   <= '1' & TX_REG(7 downto 1);
            BIT_CNTR <= BIT_CNTR + 1;
           end if;
         end if;
        when PARITY =>
         UART_TX <= '1';
         OS_CNTR <= OS_CNTR + 1;
         if OS_CNTR = 15 then
          OS_CNTR <= 0;
          state <= STOP;
       end if;  
        when STOP =>
          UART_TX <= '1';
          OS_CNTR <= OS_CNTR + 1;
          if OS_CNTR = 15 then
           OS_CNTR <= 0;
           TX_DONE <= '1';
           state   <= IDLE;
          end if;
       end case;
      end if;
    end if;
end process;
end;
