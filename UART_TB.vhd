-- UART Project - Top level testbench
-- Daniel Borquaye - DB363
-- 28/11/2014

---------------------------------------------------------------
-- Library Statements
---------------------------------------------------------------
--
library ieee;
library std;
library std_developerskit;

use std_developerskit.std_iopak;      
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use ieee.std_logic_unsigned.ALL;
use ieee.std_logic_arith.all;

---------------------------------------------------------------
-- Entity Statement
---------------------------------------------------------------

entity 	UART_TB is
end 	UART_TB;


---------------------------------------------------------------
-- TestBench Architecture
---------------------------------------------------------------

architecture behavior OF UART_TB is 


---------------------------------------------------------------
-- Component Description
---------------------------------------------------------------
component UART is
  port (
         START_TX, CLK, ReX, CLR : in std_logic;
         DIN                     : in std_logic_vector(7 downto 0);
  
         Q                       : out std_logic_vector(7 downto 0);
         TX_DONE, RX_DONE        : out std_logic);
end component;


---------------------------------------------------------------
-- signal declarations 
---------------------------------------------------------------
signal START_TX, CLK, ReX, CLR, TX_DONE, RX_DONE    : std_logic;
signal DIN, Q                                       : std_logic_vector(7 downto 0);
constant CP                                         : time := 20 ns;  
constant BG                                         : time := CP*163;

---------------------------------------------------------------
-- Component Instantiation
---------------------------------------------------------------
-- The Top level is currently described to execute on the board.
-- For the the receive cycle to be displayed by the testbench the UART_TX and ReX mappings need to be changed in the UART top level so that UART_TX drives ReX
-- This can be done by adding a signal to the top level i.e "loopback" and in the port map for TX map UART_TX to loopback and in the RX port map map ReX to loopback
-- Run for about 300 us
  
begin 
  
  uUART: UART port map(
    CLK => CLK,
    ReX => ReX,
    TX_DONE => TX_DONE,
    Q => Q,
    DIN => DIN,
    START_TX => START_TX,
    CLR => CLR);
    
-- clock signal
  process
   begin
    CLK <= '0';
    wait for CP / 2;
    CLK <= '1';
    wait for CP / 2;
  end process;
    
  process
  begin
 -- Set reset high initially to make sure all values are at default, qait for 1 clock cycle then set reset low
    CLR <= '1';
    wait for CP; CLR <= '0';
 -- Drive some idle state values to inputs so nothing happens 
    ReX <= '1'; DIN <= x"ff"; START_TX <= '0';

 -- Wait 1 clock cycle then set the input data
    wait for CP; DIN <= "11110000";
 -- Wait 1 baud clock cycle then start the transmitter
    wait for BG; START_TX <= '1';
 -- Wait 1 baud clock cycle then set the start transmitter signal low and leave it low
    wait for BG; START_TX <= '0';
    wait;
  end process;

end;
    