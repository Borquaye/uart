-- UART Project - Top level
-- Daniel Borquaye - DB363
-- 28/11/2014

----------------------------------------------------------
-- Library Statements
----------------------------------------------------------

library ieee;
    
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_unsigned.all;

----------------------------------------------------------
-- Entity Declaration
----------------------------------------------------------

entity UART is
  port(
       START_TX, CLK, ReX, CLR : in std_logic;
       DIN                     : in std_logic_vector(7 downto 0);

       Q                       : out std_logic_vector(7 downto 0);
       TX_DONE, RX_DONE, TXO   : out std_logic);

end UART;
----------------------------------------------------------
-- Architecture Declaration
----------------------------------------------------------

architecture functional of UART is

-- Baud generator component  
  component BAUDGEN
    generic( 
            br: integer);
    port   (
            CLK, RES : in std_logic;
            CE       : out std_logic);
  end component;
------------------------------------------
-- Transmitter component 
  component TX
    port(
         START_TX, CE, CLK, RES : in std_logic;
         D                      : in std_logic_vector(7 downto 0);

         UART_TX, TX_DONE       : out std_logic);
  end component;
------------------------------------------

-- Receiver component
  component RX
    port(
         ReX, CE, CLK, RES : in std_logic;
    
         Q                 : out std_logic_vector(7 downto 0);
         RX_DONE           : out std_logic);
  end component;

----------------------------------------------------------
-- Signal Declarations
----------------------------------------------------------
-- Intermediary signals that inputs/ouputs of components need for mapping
 
signal RES_i, CE_i, CLR_i : std_logic;
-- signal loopback        : std_logic;

begin
  
CLR_i <= CLR;

-- Map ports for the baud generator
  Baud_Generator: BAUDGEN
    generic map(
                br => 162)
    port map   (
                CLK => CLK,
                RES => RES_i,
                CE  => CE_i);
------------------------------------------  
-- Map ports for the transmitter
  Transmitter: TX
    port map   (
                D        => DIN,  
                CLK      => CLK,
                START_TX => START_TX,
                CE       => CE_i,
                RES      => RES_i,
                UART_TX  => TXO, --loopback,
                TX_DONE  =>  TX_DONE);
-------------------------------------------
-- Map ports for the receiver
  Receiver: RX
    port map   (
                CLK     => CLK, 
                ReX     => ReX, --loopback,
                CE      => CE_i,
                RES     => RES_i,
                Q       => Q,
                RX_DONE => RX_DONE);
                
-- Synchronise reset with the system clock                
Reset: process(CLK)
        begin
          if rising_edge(CLK) then
            RES_i <= CLR_i;
        end if;
      end process;

end functional;
