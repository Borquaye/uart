-- UART Project - Baud generator
-- Daniel Borquaye - DB363
-- 28/11/2014

----------------------------------------------------------
-- Library Statements
----------------------------------------------------------

library ieee;
    
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_unsigned.all;

----------------------------------------------------------
-- Entity Declaration
----------------------------------------------------------

-- Setting the inputs and outputs of the Baud Generator. 
-- also passed a generic integer that will need to be passed to the entity as a multiplier for the clock 

entity BAUDGEN is 
  generic( 
          br: integer);
  port   (
          CLK, RES : in std_logic;
          CE       : out std_logic);
  end BAUDGEN;
        
----------------------------------------------------------
-- Architecture Declaration
----------------------------------------------------------
          
architecture functional of BAUDGEN is

----------------------------------------------------------
-- Signal Declarations
----------------------------------------------------------

-- This signal is used to count the number of clocks that have passed.
-- It has a limit of the generic integer declared in the entity declaration.
-- This value will be set in the top level "UART"

signal count  : integer range 0 to br;

 begin

-- The statement before the process sets the output "CE" high when the counter has reached the desired limit.  
CE <= '1' when count = br else '0';

-- This process incriments the counter every time the system clock goes high and resets when the desired value is reached.
-- Reset the counter when the reset is high.
  process(CLK)
   begin
       if rising_edge(CLK) then
        if RES = '1' then
         count <= 0;
        end if;
        if count = br then
           count <= 0;
        else
           count <= count + 1;
        end if;
       end if;
  end process;
end;