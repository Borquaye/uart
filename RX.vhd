-- UART Project - Receiver
-- Daniel Borquaye - DB363
-- 28/11/2014

----------------------------------------------------------
-- Library Statements
----------------------------------------------------------

library ieee;
    
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_unsigned.all;

----------------------------------------------------------
-- Entity Declaration
----------------------------------------------------------

entity RX is 

  port (
    ReX, CE, CLK, RES : in std_logic;
    
    Q                : out std_logic_vector(7 downto 0);
    RX_DONE          : out std_logic);
  end RX;
        
----------------------------------------------------------
-- Architecture Declaration
----------------------------------------------------------
          
architecture functional of RX is

----------------------------------------------------------
-- Signal Declarations
----------------------------------------------------------
signal RX_SYNC             : std_logic;
signal TEMP                : std_logic_vector(7 downto 0);
signal DBITS               : integer range 0 to 8; 
signal OS_CNTR             : integer range 0 to 16;

-- Declaration of the 5 states the system can enter in the state machine
type xx_state is (IDLE, START, DATA, PARITY, STOP);

-- Sets the initial state to IDLE
signal state : xx_state := IDLE;


begin

-- Drive singal RX_sync with input ReX on the positive edge of the clock 
Sync: process(CLK)
   begin
    if rising_edge(CLK) then

     if RES = '1' then
      RX_SYNC <= '1';
     else
      RX_SYNC <= ReX;
     end if;
    end if;
end process;

-- State machine process. The process uses a signal OS_CNTR which is used to oversample the data to make sure it is stable
State_Machine: process(CLK, CE) 
   begin
   if rising_edge(CLK) then
    if RES = '1' then
     state <= IDLE;
     TEMP  <= "11111111";
     Q     <= "00000000";
    end if;
     if CE = '1' then
      case state is
    -- Setting the signal values for the idle state, setting all the counters to 0 and rx_done signal low
        when IDLE =>
         DBITS    <= 0;
         OS_CNTR  <= 0;
         RX_DONE  <= '0';
         if RX_SYNC = '0' then
          state <= START;
         end if;
    -- After 7 samples of the baud clock if the start bit is present then begin
        when START =>
         OS_CNTR <= OS_CNTR + 1;
         if OS_CNTR = 7 then 
          if ReX = '0' then
            OS_CNTR <= 0;
            state  <= DATA;
           else
             state <= IDLE;
           end if;
         end if;
    -- After 15 samples of the baud clock shift the 1 bit data into the 8 bit register and incriment the count until 8 bits have been shifted in
        when DATA =>
         OS_CNTR <= OS_CNTR + 1;
          if OS_CNTR = 15 then
           if DBITS = 8 then
            OS_CNTR <= 0;
            state <= PARITY;
           else
            OS_CNTR <= 0;
            TEMP(7 downto 0) <= ReX & TEMP(7 downto 1);
            DBITS            <= DBITS + 1;
           end if;
          end if;
        when PARITY =>
          OS_CNTR <= OS_CNTR + 1;
          if OS_CNTR = 15 then
            OS_CNTR <= 0;
            state <= STOP;
          end if;
        when STOP =>
          OS_CNTR <= OS_CNTR + 1;
          if OS_CNTR = 15 then
           if ReX = '1' then
            Q       <= TEMP; 
            RX_DONE <= '1';
            state   <= IDLE;
           else 
            state   <= IDLE;
         end if;
        end if;
       end case;
      end if;
     end if;
end process;
end;

